# Board class represents "Tic-tac-toe" game board, which is 2d array. 
# It consists with 9 fields on which can be placed cross or circle character.
class Board
  def initialize
    @board = Array.new(3){Array.new(3, 0)}
  end

  # Returns actual state of board
  def board
    @board.freeze
  end

  # Places the pawn on a field of a given position
  #
  # === Params:
  # +pawn+:: character which represents pawn on the field. It can be "x", "X", "o" or "O"
  # +position+:: Position object which represents position on the field of the given pawn.
  #
  # === Returns:
  # - *True* if the given position is free
  # - *False* if on the given position is already another pawn
  def make_move(pawn, position)
    @board[position[:x]][position[:y]] = pawn
  end

  # === Returns:
  # - *True* if board has free positions on which can be placed pawn
  # - *False* if there is no space on the board 
  def empty_space?
    @board.each do |row|
      return True if @board.include?(nil)
    end
    False
  end
end

