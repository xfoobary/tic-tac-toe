class Game
  def initialize
    @board = Board.new
    @players = [player1, player2]
    @player_turn = player1
  end
  
  def make_move(position)
    @board.make_move(@player_turn.pawn, position)
      
    end
  end
end
